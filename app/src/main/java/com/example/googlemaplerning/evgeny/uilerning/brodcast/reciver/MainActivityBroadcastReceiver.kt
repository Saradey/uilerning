package com.example.googlemaplerning.evgeny.uilerning.brodcast.reciver

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.googlemaplerning.evgeny.uilerning.R
import kotlinx.android.synthetic.main.activity_brodcast.*

class MainActivityBroadcastReceiver : AppCompatActivity() {

    lateinit var receiver: MainReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brodcast)


        //ресивер также можно создавать в коде
//        registerReceiver(MainReceiver(), IntentFilter())
        //отписать ресивер
//        unregisterReceiver(MainReceiver)
        receiver = MainReceiver()
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter().apply {
            this.addAction("com.example.MY_NOTIFICATION")
        })

        btnReciver.setOnClickListener {
            val intent = Intent()
            intent.action = "com.example.MY_NOTIFICATION"
            intent.putExtra("data", "Hello Reciver")
//            sendBroadcast(intent)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        }
    }


    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }
}