package com.example.googlemaplerning.evgeny.uilerning.brodcast.reciver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast


class MainReceiver : BroadcastReceiver() {


    //выполняет в ui потоке
    override fun onReceive(context: Context?, intent: Intent?) {
        val sb = StringBuilder()
        sb.append("Action: " + intent?.action.toString() + "\n")
        sb.append("URI: " + intent?.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n")
//        sb.append("extra ${intent?.getStringExtra("data")}")
        val log = sb.toString()
        Toast.makeText(context, log, Toast.LENGTH_LONG).show()
    }


}