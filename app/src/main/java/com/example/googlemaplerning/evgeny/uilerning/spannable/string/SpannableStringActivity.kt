package com.example.googlemaplerning.evgeny.uilerning.spannable.string

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.googlemaplerning.evgeny.uilerning.R
import kotlinx.android.synthetic.main.activity_spannable_string.*

class SpannableStringActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spannable_string)

        val resultSpan = SpannableString("тестовый текст тра ля ля")

        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_android_black)
        drawable?.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val imageSpan = ImageSpan(drawable!!, ImageSpan.ALIGN_BASELINE)

        resultSpan.setSpan(
            imageSpan,
            10,
            11,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        txvSpannableTest.text = resultSpan
    }


}