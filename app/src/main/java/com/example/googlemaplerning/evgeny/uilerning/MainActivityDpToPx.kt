package com.example.googlemaplerning.evgeny.uilerning

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout

class MainActivityDpToPx : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = LayoutInflater.from(this)
            .inflate(R.layout.activity_main_dp_to_px, null, false)
        setContentView(view)

        //как узнать плотность пикселей экрана
        (view as ConstraintLayout).post {
            Log.d("MainActivityDpToPx", "${Looper.myLooper() == Looper.getMainLooper()}")
            val display = this.windowManager.defaultDisplay
            val displayMetrics = DisplayMetrics()
            display?.getMetrics(displayMetrics)

            displayMetrics.widthPixels
            displayMetrics.heightPixels

            //маленький экран
            //360 x
            //604 y

            //большой экран
            //411
            //683
            val dpX = displayMetrics.widthPixels / displayMetrics.density
            val dpY = displayMetrics.heightPixels / displayMetrics.density
        }


    }


}
