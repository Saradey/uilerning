package com.example.googlemaplerning.evgeny.uilerning.animation

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.uilerning.R
import kotlinx.android.synthetic.main.activity_animation.*

class ActivityAnimation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)

        val animator = ValueAnimator.ofFloat(0f, 100f)
        animator.duration = 1000
        animator.interpolator = DecelerateInterpolator()
        animator.addUpdateListener {
            val newRadius = it.animatedValue as Float
            imvTestRippleEffect.translationX = newRadius
        }

        animator.start()
    }


}



